#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "scramble.h"

int main(int argc, char *argv[])
{
    printf("****************************************\n");
    printf("***     NAND SCRAMBLER EMULATOR      ***\n");
    printf("****************************************\n");

    if(argc != 2){
        printf("Usage  : %s data_file\n", argv[0]);
        printf("Exmaple: %s scramble\n", argv[0]);
        return 0;
    }
    /*origin host data*/
    LFSR_TYPE* user_data       = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    /*scrambled data*/
    LFSR_TYPE* scrambled_data  = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    /*lfsr generate mode data*/
    LFSR_TYPE* scrambling_mode   = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    /*descrambled data*/
    LFSR_TYPE* descrambled_data = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    size_t get_ok                  = 0;
    get_ok                         = get_data_from_file(argv[1], DATA_SIZE, user_data);
    /* reverse_data_bits(user_data); */
    assert(get_ok != 0);

    lfsr(scrambling_mode,SEED_8_BIT,MASK_8_BIT);
    scramble(user_data, scrambling_mode,scrambled_data);
    descramble(scrambled_data, scrambling_mode, descrambled_data);
    /* count zero and one */
    int scrambling_zero = 0, scrambling_one = 0;
    count_bit(scrambling_mode,&scrambling_zero, &scrambling_one);
    int user_zero = 0, user_one = 0;
    count_bit(user_data,&user_zero, &user_one);
    int scrambled_zero = 0, scrambled_one= 0;
    count_bit(scrambled_data,&scrambled_zero, &scrambled_one);
    int descrambled_zero = 0, descrambled_one= 0;
    count_bit(descrambled_data,&descrambled_zero, &descrambled_one);
    /* note: the end of string is '\0' */
#if DEBUG
    char user[LFSR_LEN+1],scrabled[LFSR_LEN+1],scrambling[LFSR_LEN+1],descrambled[LFSR_LEN+1];
    printf("BYTE USERDATA+SCRAMING=SCRAMBED DESCRAMB\n");
    for(int i = 0; i < DATA_SIZE; i++)
    {
        toBinary(user, user_data[i]);
        toBinary(scrabled, scrambled_data[i]);
        toBinary(scrambling, scrambling_mode[i]);
        toBinary(descrambled, descrambled_data[i]);
        printf("%4d %s %s %s %s\n",i,user, scrambling, scrabled, descrambled);
    }
#endif
    printf("-----------------COUNT-----------------\n");
    printf("BYTE USERDATA SCRAMING SCRAMBED DESCRAMB\n");
    printf("ONES:%8.d %8.d %8.d %8.d\n",user_one, scrambling_one, scrambled_one,descrambled_one);
    printf("ZERO:%8.d %8.d %8.d %8.d\n",user_zero, scrambling_zero, scrambled_zero, descrambled_zero);
    printf("PCT0:%8.3f %8.3f %8.3f %8.3f\n",(float)user_zero/(user_one+user_zero), \
            (float)scrambling_zero/(scrambled_one+scrambled_one),\
            (float)scrambled_zero/(scrambled_one+scrambled_zero),\
            (float)descrambled_zero/(descrambled_one+descrambled_zero));
    int zero_user_data = find_max_consecutive_zero_number(user_data);
    int zero_scrambling_data = find_max_consecutive_zero_number(scrambling_mode);
    int zero_scrambled_data = find_max_consecutive_zero_number(scrambled_data);
    int zero_descrambled_data = find_max_consecutive_zero_number(descrambled_data);
    printf("CTN0:%8.d %8.d %8.d %8.d\n",zero_user_data, zero_scrambling_data, zero_scrambled_data,zero_descrambled_data);

    free(user_data);
    free(scrambling_mode);
    free(descrambled_data);
    free(scrambled_data);

}
