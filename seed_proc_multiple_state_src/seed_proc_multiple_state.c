#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "scramble.h"

int main(int argc, char *argv[])
{
    printf("****************************************\n");
    printf("***     NAND SCRAMBLER EMULATOR      ***\n");
    printf("****************************************\n");

    if(argc != 2){
        printf("Usage  : %s data_file\n", argv[0]);
        printf("Exmaple: %s scramble\n", argv[0]);
        return 0;
    }
    /* LFSR_TYPE* user_data[PAGES_PER_WORDLINE],*scrambled_data[PAGES_PER_WORDLINE],\ */
    /*     *scrambling_mode[PAGES_PER_WORDLINE]; */
    LFSR_TYPE* user_data,*user_data_mp,*scrambled_data,*scrambled_data_mp,*scrambling_mode;
    /*origin host data*/
    user_data_mp        = calloc(WORDLINE_DATA_SIZE,sizeof(LFSR_TYPE));
    /*scrambled data*/
    scrambled_data_mp   = calloc(WORDLINE_DATA_SIZE,sizeof(LFSR_TYPE));
    /*lfsr generate mode data*/
    scrambling_mode     = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    size_t get_ok       = 0;
    get_ok              = get_data_from_file(argv[1], WORDLINE_DATA_SIZE, user_data_mp);
    assert(get_ok      != 0);
    for (int i = 0; i < PAGES_PER_WORDLINE; ++i) {
        user_data = user_data_mp + i * DATA_SIZE;
        scrambled_data = scrambled_data_mp + i * DATA_SIZE;
        lfsr(scrambling_mode,SEED_8_BIT+i,MASK_8_BIT);
        scramble(user_data, scrambling_mode,scrambled_data);
        /* count zero and one */
        int scrambling_zero = 0, scrambling_one = 0;
        count_bit(scrambling_mode,&scrambling_zero, &scrambling_one);
        int user_zero = 0, user_one = 0;
        count_bit(user_data,&user_zero, &user_one);
        int scrambled_zero = 0, scrambled_one= 0;
        count_bit(scrambled_data,&scrambled_zero, &scrambled_one);
        /* note: the end of string is '\0' */
        printf("----------PAGE %d COUNT------------\n",i);
        printf("BYTE USERDATA SCRAMING SCRAMBED\n");
        printf("ONES:%8.d %8.d %8.d \n",user_one, scrambling_one, scrambled_one);
        printf("ZERO:%8.d %8.d %8.d \n",user_zero, scrambling_zero, scrambled_zero);
        printf("PCT0:%8.3f %8.3f %8.3f \n",(float)user_zero/(user_one+user_zero), \
                (float)scrambling_zero/(scrambled_one+scrambled_one),\
                (float)scrambled_zero/(scrambled_one+scrambled_zero));
        int zero_user_data = find_max_consecutive_zero_number(user_data);
        int zero_scrambling_data = find_max_consecutive_zero_number(scrambling_mode);
        int zero_scrambled_data = find_max_consecutive_zero_number(scrambled_data);
        printf("CTN0:%8.d %8.d %8.d \n",zero_user_data, zero_scrambling_data,zero_scrambled_data);
    }
    int state_count[STATE_NUMBER];
    // DATA_SIZE BYTE
    int total = 0;
    for (int i = 0; i < DATA_SIZE; ++i) {
        // BYTE
        for (unsigned int j = 0; j < sizeof(LFSR_TYPE) * 8; ++j) {
            // STATE
            int state = 0;
            for (int k = 0; k < PAGES_PER_WORDLINE; ++k) {
                int idx = k * DATA_SIZE+i+j;
                state = state | (scrambled_data_mp[idx] & 0x1) << k;
                scrambled_data_mp[k*DATA_SIZE+i] >>= 1;
            }
            assert(state<STATE_NUMBER);
            state_count[state]++;
            total++;
        }
    }
    printf("-----------------------------------\n");
    printf(" STATE   COUNT   PERCENT\n");
    for (int i = 0; i < STATE_NUMBER; ++i) {
        printf("%4d %8.d %8.3f\n", i, state_count[i], (float)state_count[i]/BITLINE );
    }
    printf(" COUTN BITS(%d) == BITLINE(%d)\n", total,BITLINE);
    
    free(user_data_mp);
    free(scrambling_mode);
    free(scrambled_data_mp);
}
