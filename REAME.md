

seed_proc_multiple_state
0. Introduction
    this is for test multiple state nand cell scrambler.
1. Compile
    change Macro PAGES_PER_WORDLINE = TLC or QLC
    use Makefile to compile:
    make seed_proc_multiple_state
2. Run
    ./seed_proc_multiple_state filename
    file should be bigger than 200KB.

