# compiler
CC=gcc
# CFLAGS_OPTIMIZATION ?= -O3
CFLAGS_OPTIMIZATION=
CFLAGS_WARNING ?= -Wall -Wextra -Wno-unused-parameter -Werror
# linker
LDFLAGS += $(LDLIBS)

CFLAGS += $(CFLAGS_WARNING) $(CFLAGS_OPTIMIZATION)

# src file
LIB = $(wildcard lib/*.c)
# header file
HEADER = $(wildcard inc/*.h)
# different project
SEED_PICK_SRC = $(wildcard seed_pick_src/*.c)
SEED_PROC_SRC = $(wildcard seed_proc_src/*.c)
SEED_PROC_MULTIPLE_STATE_SRC = $(wildcard seed_proc_multiple_state_src/*.c)
SEED_PICK_FOR_BITLINE_SRC = $(wildcard seed_pick_for_bitline_src/*.c)
MACROS = -D DEBUG 
# header
INCLUDE = -Iinc

# excuteble
SEED_PICK = seed_pick
SEED_PROC = seed_proc
SEED_PICK_FOR_BITLINE = seed_pick_for_bitline
SEED_PROC_MULTIPLE_STATE = seed_proc_multiple_state
default : $(SEED_PICK) $(SEED_PROC) $(SEED_PROC_MULTIPLE_STATE)

$(SEED_PICK) : $(LIB) $(SEED_PICK_SRC) $(HEADER)
	$(CC) $(CFLAGS) $(INCLUDE) $(LIB) $(SEED_PICK_SRC) $(LDLIBS) -o $@ 

$(SEED_PROC) : $(LIB) $(SEED_PROC_SRC) $(HEADER)
	$(CC) $(CFLAGS) $(INCLUDE) $(LIB) $(SEED_PROC_SRC) $(LDLIBS) -o $@

$(SEED_PICK_FOR_BITLINE) : $(LIB) $(SEED_PICK_FOR_BITLINE_SRC) $(HEADER)
	$(CC) $(CFLAGS) $(INCLUDE) $(LIB) $(SEED_PICK_FOR_BITLINE_SRC) $(LDLIBS) -o $@ 

$(SEED_PROC_MULTIPLE_STATE) : $(LIB) $(SEED_PROC_MULTIPLE_STATE_SRC) $(HEADER)
	$(CC) $(CFLAGS) $(INCLUDE) $(LIB) $(SEED_PROC_MULTIPLE_STATE_SRC) $(LDLIBS) -o $@

clean:
	rm $(SEED_PROC) $(SEED_PICK) $(SEED_PROC_MULTIPLE_STATE) $(SEED_PICK_FOR_BITLINE)
