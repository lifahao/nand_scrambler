#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "scramble.h"
#define SEARCH_BEST_SEED_FOR_BITLINE 0

int main(int argc, char *argv[])
{
    printf("****************************************\n");
    printf("***     NAND SCRAMBLER EMULATOR      ***\n");
    printf("****************************************\n");

    if(argc != 2){
        printf("Usage  : %s data_file\n", argv[0]);
        printf("Exmaple: %s scramble\n", argv[0]);
        return 0;
    }
    /*origin host data*/
    LFSR_TYPE* user_data       = calloc(BLOCK_DATA_SIZE,sizeof(LFSR_TYPE));
    /*scrambled data*/
    LFSR_TYPE* scrambled_data  = calloc(BLOCK_DATA_SIZE,sizeof(LFSR_TYPE));
    /*lfsr generate mode data*/
    LFSR_TYPE* scrambling_mode   = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    /*descrambled data*/
    size_t get_ok                  = 0;
    // read many wordline data from file
    get_ok                         = get_data_from_file(argv[1], BLOCK_DATA_SIZE, user_data);
    assert(get_ok != 0);

    LFSR_TYPE last_wl_data[DATA_SIZE];
    // wordline 0
    lfsr(scrambling_mode,SEED_8_BIT,MASK_8_BIT);
    scramble(user_data, scrambling_mode, scrambled_data);
    // store wordline 0
    int consective_bit_0_count[MAX_CONSECTIVE_ZERO_NUMBER]={0};
    int max_consective_bit_0_num = 0;
    memcpy(last_wl_data, user_data, DATA_SIZE);
    for(int wl = 1; wl < WORDLINE; wl++){
#if SEARCH_BEST_SEED_FOR_BITLINE
        // search best seed
        int max_bit_1 = 0;
        int best_seed = SEED_8_BIT_BEG;
        for(LFSR_TYPE seed = SEED_8_BIT_BEG; seed < SEED_8_BIT_END; seed++){
            // get scrambling bits
            lfsr(scrambling_mode,seed,MASK_8_BIT);
            scramble(user_data + wl * DATA_SIZE, scrambling_mode, scrambled_data + wl * DATA_SIZE);
            // compare 2 wls and get max diff
            // bit 1 diff is bigger, better
            int bit_1 = compare_scrambled_data(last_wl_data, scrambled_data + wl * DATA_SIZE);
            if(bit_1 > max_bit_1){
                max_bit_1 = bit_1;
                best_seed = seed;
            }
        }
        // find max consecutive 0 in wordline
        int consecutive_bit_0_num = find_max_consecutive_zero_number(scrambled_data+wl*DATA_SIZE);
        consective_bit_0_count[consecutive_bit_0_num]++;
        if(consecutive_bit_0_num > max_consective_bit_0_num){
            max_consective_bit_0_num = consecutive_bit_0_num;
        }
        printf("wl[%3d] seed[%3d] bit 1[%5d] consecutive[%3d]\n", wl, best_seed, max_bit_1, consecutive_bit_0_num);
        memcpy(last_wl_data,user_data+wl*DATA_SIZE, DATA_SIZE);
#else
        lfsr(scrambling_mode,SEED_8_BIT_BEG+wl,MASK_8_BIT);
        scramble(user_data + wl * DATA_SIZE, scrambling_mode, scrambled_data + wl * DATA_SIZE);
        // find max consecutive 0 in wordline
        int consecutive_bit_0_num = find_max_consecutive_zero_number(scrambled_data+ wl * DATA_SIZE);
        consective_bit_0_count[consecutive_bit_0_num]++;
        if(consecutive_bit_0_num > max_consective_bit_0_num){
            max_consective_bit_0_num = consecutive_bit_0_num;
        }
#endif
    }
    printf("CONS0  COUNT\n");
    for (int i = 0; i < MAX_CONSECTIVE_ZERO_NUMBER; ++i) {
        if(consective_bit_0_count[i] != 0)
            printf("%3d   %3d\n", i, consective_bit_0_count[i]);
    }
    printf("Max Consective 0 in wordline: %d\n", max_consective_bit_0_num);
    int max_consective_bit_0_num_in_bitline = find_max_consecutive_zero_number_in_bitline(scrambled_data);
    printf("Max Consective 0 in bitline : %d\n", max_consective_bit_0_num_in_bitline);
    
    /* for(int wl = 1; wl < WORDLINE; wl++){ */
    /* } */

    free(user_data);
    free(scrambling_mode);
    /* free(descrambled_data); */
    free(scrambled_data);

}
