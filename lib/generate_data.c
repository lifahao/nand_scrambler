#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "scramble.h"

/*
* @brief    get data from file
* @param
*           char *filename - open file name
*           int len - read data length
* @return   bool 0 fail
*                
*/
size_t get_data_from_file(char *file_name,int len, LFSR_TYPE* data){
    FILE* data_file = fopen(file_name,"rb+");
    if(!data_file){
        perror("fopen fail!");
        exit(EXIT_FAILURE);
    }
    return fread(data, len, sizeof(LFSR_TYPE), data_file);
}
