#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include "scramble.h"
/*
 * @brief		 k-bit LFSR(Linear Feedback Shift Register) simulater
 *               short program to understand linear feed shift register(LFSR)
 *               you can use 8bit seed to make n bits length randomized binary vector
 *               in this sample, we use seed 0xb4 to produce 2048 bits binary vector,
 *               and bit mask is 0b01110001,(means left bit = B0 XOR B4 XOR B5 XOR B6)
 *               --------
 *               ___XOR__
 *                |||   |
 *               x         output  | how to get x        |     hex
 *        0xb4 : 10110100
 *             ->01011010  0       (x = 0 ^ 1 ^ 1 ^ 0 = 0)     0x5a
 *             ->00101101  0       (x = 1 ^ 0 ^ 1 ^ 0 = 0)     0x2d
 *             ->00010110  1       (x = 0 ^ 1 ^ 0 ^ 1 = 0)     0x16
 *             ->10001011  0       (x = 0 ^ 0 ^ 1 ^ 0 = 1)     0x8b
 *             ->11000101  1       (x = 0 ^ 0 ^ 0 ^ 1 = 1)     0xc5
 *             ->01100010  1       (x = 1 ^ 1 ^ 0 ^ 0 = 0)     0x62
 *             ->00110001  0       (x = 1 ^ 1 ^ 0 ^ 0 = 0)     0x31
 *             ->10011000  1       (x = 0 ^ 0 ^ 1 ^ 0 = 1)     0x98
 *               LFSR shift 8 bit, curret value is 0x98,output value is 0x2d(0b00101101)
 *
   @param[in]	 user_data is pointer to host user data
   @param[in] 	 scrambled_data is pointer to result from xor user_data with
                 randomized data.
 */

void lfsr(LFSR_TYPE *scrambling_data,int seed, int mask_bit)
{
    LFSR_TYPE in_s, cs, cp, p, nbit,output;
    int i, j, k=0;

    cs = in_s = seed;     /*seed value, this can be any 8 bit value */
    p = mask_bit;    /* max length polynomial x^8+x^4+x^3+x^2+1 = 0b01110001 */

    while (k < STREAM) {
        output = 0;
        for (j = 0;j < LFSR_LEN;j++,k++) {
            cp = nbit = cs & p; /* mask bit */
            for (i = 1;i < LFSR_LEN; i++) { /* xor all bits together */
                nbit ^= (cp >> i);
            }
            output = (output << 1) ;
            output |=  (cs & 0x01); /*last bit initial state*/
            cs = (cs >> 1) | (nbit << 7); /*  rotate in new bit */
        }
        /* k is bits, index is Byte,from 0 */
        int index = k / LFSR_LEN - 1;
        scrambling_data[index] = output;
        /* when initial state happen again,it means LFSR circle */
        if (cs == in_s) {
#if DEBUG
            printf("\nreached duplicate at %d.\n", k);
#endif
        }
    }
}
