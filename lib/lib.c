#include <stdlib.h>
#include <stdio.h>
#include "scramble.h"

void toBinary(char *binary, LFSR_TYPE n)
{
    int k = 0;
    for (unsigned i = (1 << (LFSR_LEN- 1)); i > 0; i = i >> 1) {
        binary[k++] = (n & i) ? '1' : '0';
    }
    binary[k] = '\0';
}

/*
* @brief   count zero in binary data
* @param
*          const LFSR_TYPE* binary_data - TODO
*          int *number_zero - return value
*          int *number_one  - return value
*/
void count_bit(const LFSR_TYPE* binary_data, int *number_zero, int *number_one){

    for (int i = 0; i < DATA_SIZE; ++i) {
        LFSR_TYPE data = binary_data[i];
        for (size_t j = 0; j < sizeof(LFSR_TYPE) * 8; ++j) {
            if(data & 0x1){
                (*number_one)++;
            }
            else{
                (*number_zero)++;
            }
            data = data >> 1;
        }
    }
}

/**
 * @brief     find max consecutive 0/1 in binary array
 * @param     const LFSR_TYPE* binary_data - TODO
 * @return    int
 */
int find_max_consecutive_zero_number(const LFSR_TYPE* binary_data){
    int number_count = 0, max_number_count = 0;
    /* move from last to first*/
    for (int i = DATA_SIZE - 1; i >= 0; --i) {
        LFSR_TYPE data = binary_data[i];
        /* 0110 0001 8 bit only shift 7 times*/
        for (size_t j = 0; j < sizeof(LFSR_TYPE) * 8 ; ++j) {
            if(data & 0x1){
                number_count = 0;
            }
            else{
                number_count++;
                /* printf("i:%d j:%d c:%d\n", i,j,number_count); */
                max_number_count = number_count > max_number_count ? number_count : max_number_count;
            }
            data = data >> 1;
        }
    }
    return max_number_count;

}

int find_max_consecutive_zero_number_in_bitline(LFSR_TYPE* data){
    int max_number_count = 0;
    int number_count = 0;
    for (int i = 0; i < DATA_SIZE - 1; ++i) {
        // bit line 
        number_count = 0;
        // loop 8 bits
        for(int k = 0; k < LFSR_LEN; k++){
            number_count = 0;
            int tmp = 1 << k;
            for (int j = 0; j < WORDLINE - 1; ++j) {
                // if bit is 1,then set number_count to 0
                if(data[DATA_SIZE*j+i] & tmp){
                    number_count = 0;
                }
                else{
                    number_count++;
                    max_number_count = number_count > max_number_count ? number_count : max_number_count;
                }
            }
        }
    }
    return max_number_count;
}

void reverse_data_bits(LFSR_TYPE *data){
    for (int i = 0; i < DATA_SIZE; ++i) {
        data[i] = ~data[i];
    }
}
