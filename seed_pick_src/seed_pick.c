#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <assert.h>
#include "scramble.h"

int main(int argc, char *argv[])
{
    printf("****************************************\n");
    printf("***     NAND SCRAMBLER EMULATOR      ***\n");
    printf("****************************************\n");

    if(argc != 2){
        printf("Usage  : %s data_file\n", argv[0]);
        printf("Exmaple: %s scramble\n", argv[0]);
        return 0;
    }
    /*origin host data*/
    LFSR_TYPE* user_data       = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    /*scrambled data*/
    LFSR_TYPE* scrambled_data  = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    /*lfsr generate mode data*/
    LFSR_TYPE* scrambling_mode   = calloc(DATA_SIZE,sizeof(LFSR_TYPE));
    /*descrambled data*/
    /* LFSR_TYPE* descrambled_data = calloc(DATA_SIZE,sizeof(LFSR_TYPE)); */
    size_t get_ok                  = 0;
    get_ok                         = get_data_from_file(argv[1], DATA_SIZE, user_data);
    /* reverse_data_bits(user_data); */
    assert(get_ok != 0);

    int circle = 0;
    int user_zero = 0, user_one = 0;
    count_bit(user_data,&user_zero, &user_one);
    int consecutive_user_bit0 = find_max_consecutive_zero_number(user_data);
    printf("USER DATA\nALL BIT:%d\nBIT0:%d\nBIT1:%d\nCONSECUTIVE 0:%d\n",
            BITLINE, user_zero, user_one, consecutive_user_bit0);
    printf("------INTRODUCTION-----\n");
    printf("SEED: 8 BITS NUMBER\n");
    printf("MASK: 8 BITS NUMBER\n");
    printf("USER: USER DATA)\n");
    printf("SCIN: SCRAMBLING DATA\n");
    printf("SCED: SCRAMBLED DATA\n");
    printf("df  : bit0 - bit1\n");
    printf("cs  : consecutive 0 \n");
    printf("-----------------------\n");
    printf("SEED MASK SCINdf SCINcs SCEDdf SCEDcs\n");
    scramble_result all[SEED_8_BIT_STEP*MASK_8_BIT_STEP];
    for(LFSR_TYPE seed = SEED_8_BIT_BEG; seed < SEED_8_BIT_END; seed++){
        for(LFSR_TYPE mask = MASK_8_BIT_BEG; mask < MASK_8_BIT_END; mask++){
            int scrambling_zero = 0, scrambling_one = 0;
            int scrambled_zero = 0, scrambled_one= 0;
            lfsr(scrambling_mode,seed,mask);
            scramble(user_data, scrambling_mode,scrambled_data);
            /* descramble(scrambled_data, scrambling_mode, descrambled_data); */
            /* count zero and one */
            count_bit(scrambling_mode,&scrambling_zero, &scrambling_one);
            count_bit(scrambled_data,&scrambled_zero, &scrambled_one);
            all[circle].diff_scrambling_data = scrambling_zero - scrambling_one;
            all[circle].diff_scrambled_data = scrambled_zero - scrambled_one;

            all[circle].consecutive_scrambled_bit0
                = find_max_consecutive_zero_number(scrambled_data);
            all[circle].consecutive_scrambling_bit0
                = find_max_consecutive_zero_number(scrambling_mode);
            all[circle].seed = seed;
            all[circle].mask = mask;

            circle ++;
        }
    }
    circle = 0;
    for(LFSR_TYPE seed = SEED_8_BIT_BEG; seed < SEED_8_BIT_END; seed++){
        for(LFSR_TYPE mask = MASK_8_BIT_BEG; mask < MASK_8_BIT_END; mask++){
            printf("0x%2.x 0x%2.x %6.1d %6.1d %6.1d %6.1d\n",
                     all[circle].seed,all[circle].mask,
                     all[circle].diff_scrambling_data,
                     all[circle].consecutive_scrambling_bit0,
                     all[circle].diff_scrambled_data,
                     all[circle].consecutive_scrambled_bit0);
            circle++;
        }
    }



    free(user_data);
    free(scrambling_mode);
    /* free(descrambled_data); */
    free(scrambled_data);

}
