#include <stdlib.h>
#define BITLINE 16384
#define SLC 1
#define MLC 2
#define TLC 3
#define QLC 4
#define XLC 5
#define PAGES_PER_WORDLINE QLC
#define STATE_NUMBER (1 << PAGES_PER_WORDLINE)
#define WORDLINE 128
#define LFSR_LEN 8
#define LFSR_MAX ((1 << LFSR_LEN) -1)
#define DATA_SIZE ( BITLINE / LFSR_LEN )/*PAGE DATA SIZE BYTE */
#define BLOCK_DATA_SIZE ( WORDLINE * DATA_SIZE )
#define WORDLINE_DATA_SIZE (DATA_SIZE * PAGES_PER_WORDLINE)
#define SEED_8_BIT 0xb4
#define MASK_8_BIT 0x71
#define SEED_8_BIT_BEG 0x1
#define SEED_8_BIT_STEP 252
#define SEED_8_BIT_END (SEED_8_BIT_BEG+SEED_8_BIT_STEP)
#define MASK_8_BIT_BEG 0x71
#define MASK_8_BIT_STEP 1
#define MASK_8_BIT_END (MASK_8_BIT_BEG + MASK_8_BIT_STEP)
#define MAX_CONSECTIVE_ZERO_NUMBER 50
#define STREAM ( DATA_SIZE * LFSR_LEN)

typedef unsigned char LFSR_TYPE;
typedef struct{
    LFSR_TYPE seed;
    LFSR_TYPE mask;
    // record (0-1)'s number
    int diff_user_data;
    int diff_scrambling_data;
    int diff_scrambled_data;
    // record consecutive 0's number
    int consecutive_user_bit0;
    int consecutive_scrambling_bit0;
    int consecutive_scrambled_bit0;
} scramble_result;

void lfsr(LFSR_TYPE *scrambling_data,int seed, int mask_bit);
void scramble(LFSR_TYPE *user_data, LFSR_TYPE *scrambling_data,LFSR_TYPE* scrambled_data);
void descramble(LFSR_TYPE *scrambled_data, \
        LFSR_TYPE *scrambling_data,LFSR_TYPE* descrambled_data);
size_t get_data_from_file(char *file_name,int len, LFSR_TYPE* data);
void toBinary(char *binary, LFSR_TYPE n);
void count_bit(const LFSR_TYPE* binary_data, int *number_zero, int *number_one);
void reverse_data_bits(LFSR_TYPE *data);
int find_max_consecutive_zero_number(const LFSR_TYPE* binary_data);
int compare_scrambled_data(LFSR_TYPE *user_data, LFSR_TYPE* scrambled_data);
int find_max_consecutive_zero_number_in_bitline(LFSR_TYPE* data);
